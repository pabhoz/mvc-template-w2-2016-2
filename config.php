<?php

    $_PROTOCOL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
    define('URL', $_PROTOCOL.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
    define('LIBS','libs/');
    define('MODELS','./models/');
    define('BL','./bussinesLogic/');
    define('MODULE','./views/modules/');

    define('_DB_TYPE', 'mysql');
    define('_DB_HOST' , 'localhost');
    define('_DB_USER' , 'root' );
    define('_DB_PASS' , '' );
    define('_DB_NAME' , 'myshop');

    define('HASH_ALGO' , 'sha512');
    define('HASH_KEY' , 'my_key');
    define('HASH_SECRET' , 'my_secret');
    define('SECRET_WORD' , 'so_secret');
